# qrb5165-sys-img-build-docker

QRB5165 System Image Build docker project.

NOTE: this is in ALPHA.  You can use this to see how the build environment is setup.  A build project is still required to be made public and is in the works.

## Summary

Creates a docker image that can build the QRB5165 `system image` (HLOS/kernel).

## Required

Docker

## To Use

### Build Container

Run the following the build a container locally that can build the system image.

```
./build-docker.sh
```

### Obtain Build Source

Upon request at this time (devops at modalai dot com)

For example:

```
modalai@threadripper:~/public/qrb5165-sys-img-build-docker$ ls -la
total 2830576
drwxrwxr-x 3 modalai modalai       4096 Mar  7 05:52 .
drwxrwxr-x 4 modalai modalai       4096 Mar  7 05:37 ..
-rwxrwxr-x 1 modalai modalai        398 Mar  7 05:52 build-docker.sh
-rw-rw-r-- 1 modalai modalai       3158 Mar  7 05:34 Dockerfile
drwxrwxr-x 8 modalai modalai       4096 Mar  7 05:52 .git
-rw-rw-r-- 1 modalai modalai       1646 Mar  7 05:34 LICENSE
-rw-rw-r-- 1 modalai modalai 2898472230 Mar  7 05:50 m0054-001-14.1a-lu.um.1.2.1.tar.gz
-rw-rw-r-- 1 modalai modalai        447 Mar  7 05:34 README.md
-rwxrwxr-x 1 modalai modalai        183 Mar  7 05:34 run-docker.sh
```

### Run Docker

```
./run-docker.sh
```

### Unzip build artifacts

tar -xzvf m0054-001-14.1a-lu.um.1.2.1.tar.gz
mv workspace/* .

### Build

```
./build.sh
```

### Output Location


```
/opt/workspace/build/build_mount/lu.um.1.2.1/apps_proc/build-qti-distro-ubuntu-fullstack-perf/tmp-glibc/deploy/images/qrb5165-rb5/
```
