#!/bin/bash

# Verbose mode
set -x
# Exit if any command has error
set -e

IMAGE_NAME=qrb5165-sys-img-build
MAJOR_VERSION=1
MINOR_VERSION=0

docker build -t $IMAGE_NAME:v$MAJOR_VERSION.$MINOR_VERSION .
docker tag $IMAGE_NAME:v$MAJOR_VERSION.$MINOR_VERSION $IMAGE_NAME:latest

# docker save $IMAGE_NAME:v$MAJOR_VERSION.$MINOR_VERSION | \
#     gzip > $IMAGE_NAME_v$MAJOR_VERSION\_$MINOR_VERSION.tgz
