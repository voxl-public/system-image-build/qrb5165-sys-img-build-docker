#!/bin/bash

BUILD_ID="LU.UM.1.2.1.r1-30500"
REPO_MANIFEST_FILE="LU.UM.1.2.1.r1-30500-QRB5165.0.xml"
REPO_MANIFEST_URL="https://source.codeaurora.org/quic/le/le/manifest"
REPO_URL=git://codeaurora.org/tools/repo.git
REPO_BRANCH=caf-stable

WORKSPACE="$(pwd)"

# prevent git from prompting us such that this requires no user intervention
git config --global color.ui false

git config --global user.email "foo@bar.com"
git config --global user.name "Mr. Foobar III"

echo "[INFO] - Syncing CAF repo..."
repo init -u "${REPO_MANIFEST_URL}" -b release -m "${REPO_MANIFEST_FILE}" --repo-url="${REPO_URL}" --repo-branch="${REPO_BRANCH}"
repo sync -c --no-tags -q -j"$(nproc)"

sleep 5

echo "[INFO] - Building..."

PREBUILT_SRC_DIR="${WORKSPACE}/prebuild_HY11"
MACHINE=qrb5165-rb5 
DISTRO=qti-distro-ubuntu-fullstack-debug
#export DISTRO=qti-distro-fullstack-perf
source poky/qti-conf/set_bb_env.sh

bitbake linux-msm
bitbake qti-ubuntu-robotics-image -fc do_make_bootimg
