#!/bin/bash

# see  https://gitlab.com/voxl-public/system-image-build/qrb5165-sys-img-build-docker
docker run -it --rm --privileged \
		--mount type=bind,source="$(pwd)"/workspace,target=/home/user/workspace \
		qrb5165-sys-img-build
